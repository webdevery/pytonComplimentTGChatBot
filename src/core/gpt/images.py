import openai
import core.localize as Localize

from core.manager import ChatManager
from utils import saveFile, logError, logInfo, logSuccess

def askImage(chat: ChatManager):
    user = chat.user
    prompt = chat.answer
    try:
        logInfo('askImage', 'Запрос начат', user.id, prompt)
        response = openai.Image.create(
            prompt=prompt,
            n=1,
            size="512x512"
        )

        fileName = saveFile(
            url=response['data'][0]['url'],
            name=f"{user.id}/image-{response['created']}.jpg"
        )

        logSuccess('askImage', 'Завершился для', user.id, fileName)
        return fileName
    except Exception as e:
        logError('askImage', e)
        return Localize.PROCCESSING_ERROR

def variansImage(chat: ChatManager):
    user = chat.user
    path = chat.answer
    try:
        response = openai.Image.create_variation(
            image=open(path, "rb"),
            n=1,
            size="512x512"
        )

        fileName = saveFile(
            url=response['data'][0]['url'],
            name=f"{user.id}/image-{response['created']}.jpg"
        )

        return fileName
    except Exception as e:
        logError('variansImage', e)
        return Localize.PROCCESSING_ERROR