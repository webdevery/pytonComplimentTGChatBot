import openai
import tiktoken

from typing import List
from models.types import GPTMessage
from utils import formatMessage

def getTokensCount(messages: List[GPTMessage], model):
    try:
        encoding = tiktoken.encoding_for_model(model)
    except KeyError:
        encoding = tiktoken.get_encoding("cl100k_base")
    num_tokens = 0
    for message in messages:
        num_tokens += 4  # every message follows <im_start>{role/name}\n{content}<im_end>\n
        for key, value in message.items():
            num_tokens += len(encoding.encode(value))
            if key == "role":  # if there's a name, the role is omitted
                num_tokens += -1  # role is always required and always 1 token
    num_tokens += 2  # every reply is primed with <im_start>assistant
    return num_tokens

def validateContext(messages: List[GPTMessage], limit, model):
    summ = getTokensCount(messages=messages, model=model)

    if (summ > limit):
        return validateContext(messages[1:], limit, model=model)
    else:
        return messages

def getChatCompletion(model, messages, temperature, max_tokens, onStream = None):
    completion = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature,
        max_tokens=max_tokens,
        stream=True,
    )

    answerData = ''
    for chunk in completion:
        choice = chunk.choices[0]
        if ('content' in choice.delta):
            answerData += choice.delta.content
            if (onStream != None):
                onStream(formatMessage(answerData))
    return formatMessage(answerData)