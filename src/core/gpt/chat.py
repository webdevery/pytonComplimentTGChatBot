from typing import List
from utils import logSuccess, logError, logInfo
from models.types import User, GPTMessage
from core.manager import ChatManager
import core.localize as Localize

from core.gpt.utils import validateContext, getTokensCount, getChatCompletion

model = "gpt-3.5-turbo"
temperature = 0.5
max_tokens = 1000

TOKENS_LIMIT = 4096 - max_tokens
MAX_ONETIME_REQUESTS = 2
BASE_USER_GPT_CONTEXT = 'Отвечай на любые вопросы, хвали за упорство'
BASE_SYST_GPT_CONTEXT = 'Твоя задача в каждом сообщении выказывать уважение Пользователю'

userMessages = {}
requestInProgressByUser = {}

def getUserContext(user: User) -> List[GPTMessage]:
    context: List[GPTMessage] = []
    if (user.systemGPTContext != None):
        context.append({ 'role': 'system', 'content': user.systemGPTContext})
    else:
        context.append({ 'role': 'system', 'content': BASE_SYST_GPT_CONTEXT})

    if (user.userGPTContext != None):
        context.append({ 'role': 'user', 'content': user.userGPTContext})
    else:
        context.append({ 'role': 'user', 'content': BASE_USER_GPT_CONTEXT})

    return context

def askGPT(chat: ChatManager):
    user = chat.user
    prompt = chat.answer

    userID = str(user.id)
    answer = ''
    try:
        if (userID in requestInProgressByUser and requestInProgressByUser[userID] == True):
            return 'Превышен лимит одновременных запросов к чату, дождитесь завершения текущих запросов и попробуйте снова.'
        else:
            requestInProgressByUser[userID] = True

        context = getUserContext(user=user)

        currUserMessages: List[GPTMessage] = userMessages[userID] if userID in userMessages.keys() else []
        currUserMessages.append({ "role": "user", "content": prompt })

        validatedContext = validateContext(messages=currUserMessages, limit=TOKENS_LIMIT - getTokensCount(messages=context, model=model), model=model)
        userMessages[userID] = validatedContext + []

        logInfo('askGPT', 'Запрос начат', userID, answer)
        answer = getChatCompletion(
            model=model,
            messages=context + validatedContext,
            temperature=temperature,
            max_tokens=max_tokens,
        )

        currUserMessages.append({"role": "assistant", "content": answer})
        userMessages[userID] = currUserMessages
        
        requestInProgressByUser[userID] = False
        logSuccess('askGPT', 'Запрос завершился для', userID)
    except Exception as e:
        requestInProgressByUser[userID] = False
        logError('askGPT', 'Ошибка при запросе к GPT', prompt, e)
        answer = Localize.PROCCESSING_ERROR
    finally:
        requestInProgressByUser[userID] = False

    return answer