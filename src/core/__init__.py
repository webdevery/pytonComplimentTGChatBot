from models import SessionBase
from thread import startAsync
import time

sessionBase: SessionBase = SessionBase()

def DBInit():
    def clearCache():
        sessionBase.clearCache()
        time.sleep(60)
        startAsync(clearCache)
    startAsync(clearCache)
