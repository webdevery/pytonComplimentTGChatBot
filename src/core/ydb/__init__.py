import os
import ydb
import ydb.iam
from typing import List

from utils import logSuccess, logError, logInfo, formatMessage
from models.types import User
from config import YDB_ENDPOINT, YDB_DATABASE

USERS_TABLE = 'compliment/users'
USER_FIELDS = 'id, username, first_name, last_name, name, sex, systemGPTContext, userGPTContext'

def connect():
    onYF = str(os.environ.get('YF')) == '1'
    authService = None
    if (onYF):
        authService = ydb.iam.MetadataUrlCredentials()
    else:
        authService = ydb.iam.ServiceAccountCredentials.from_file('src/config/key.json', iam_endpoint=None, iam_channel_credentials=None )
    driver = ydb.Driver(endpoint=YDB_ENDPOINT, database=YDB_DATABASE, credentials=authService)
    driver.wait(fail_fast=True, timeout=5)
    return ydb.SessionPool(driver)

def getSession():
    pool = connect()
    def returnSession(session):
        return session
    return pool.retry_operation_sync(returnSession)

def _parseStrValue(val: str) -> str:
    if (val != None):
        return f'"{formatMessage(val)}"'
    else:
        return 'null'

def _parseValue(val) -> str:
    if (val != None):
        return f'{val}'
    else:
        return 'null'

def _parseToQuery(user: User) -> str:
    userId = _parseValue(user.id)
    username = _parseStrValue(user.username)
    first_name = _parseStrValue(user.first_name)
    last_name = _parseStrValue(user.last_name)
    name = _parseStrValue(user.name)
    sex = _parseValue(user.sex)
    systemGPTContext = _parseStrValue(user.systemGPTContext)
    userGPTContext = _parseStrValue(user.userGPTContext)
    
    return f'{userId}, {username}, {first_name}, {last_name}, {name}, {sex}, {systemGPTContext}, {userGPTContext}'

def getUsers() -> List[User]:
    try:
        session = getSession()
        query = f'SELECT {USER_FIELDS} FROM `{USERS_TABLE}`'

        result_sets = session.transaction(ydb.SerializableReadWrite()).execute(query, commit_tx=True)
        rows: List[User] = result_sets[0].rows
        logSuccess('getUsers', len(rows), rows)
        def parseUser(u):
            return User(**u)

        return [parseUser(obj) for obj in rows]
    except Exception as e:
        logError('getUsers', e)
    return []

def getUser(id: str) -> User:
    try:
        session = getSession()
        query = f'SELECT {USER_FIELDS} FROM `{USERS_TABLE}` WHERE id = {id}'

        result_sets = session.transaction(ydb.SerializableReadWrite()).execute(query, commit_tx=True)
        row: User = result_sets[0].rows[0]

        return User(**row)    
    except Exception as e:
        logError('getUser', e)
        return None

def addUser(user: User) -> None:
    try:
        session = getSession()
        query = f'UPSERT INTO `{USERS_TABLE}` ({USER_FIELDS}) VALUES ({_parseToQuery(user)})'
        logInfo('addUser', query)
        session.transaction().execute(query, commit_tx=True)
        logSuccess('addUser', user)

    except Exception as e:
        logError('addUser', e)

def updateUser(user: User) -> None:
    try:
        session = getSession()
        query = f'UPSERT INTO `{USERS_TABLE}` ({USER_FIELDS}) VALUES ({_parseToQuery(user)})'
        logInfo('updateUser', query)
        session.transaction().execute(query, commit_tx=True)
        logSuccess('updateUser', user)

    except Exception as e:
        logError('updateUser', e)
