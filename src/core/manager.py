import telegram
from telegram import ChatAction
from telegram.ext import CallbackContext
import threading
import time

from thread import startAsync
from utils import logSuccess, logError, tryCatch, formatMessage, getLoadingInclicator, Log
from config import ADMIN_ID
from core import sessionBase
from core.ydb import getUsers
from models.types import User

def addUserIfUnique(newUser: User) -> User:
    if newUser.id in sessionBase.users.keys():
        return sessionBase.getUser(newUser.id)
    else:
        return sessionBase.createNewOrGetExistedUser(newUser)

PARSE_MARKDOWN = 'MarkdownV2'

class ChatManager:
    update = None
    context = None
    bot = None

    query = None
    message = None
    answer = None
    user: User = None
    logger: Log = None

    def __init__(self, update, context: CallbackContext):
        self.update = update
        self.context = context
        self.bot = context.bot

        self.message = update.message
        self.query = update.callback_query

        if (hasattr(update.message, 'text')):
            self.answer = update.message.text
        elif (hasattr(update.callback_query, 'answer')):
            self.answer = update.callback_query.answer()

        if (hasattr(update.message, 'chat')):
            self.user = addUserIfUnique(self.message.chat)
            for field in self.user.__dict__.keys():
                if (self.user.__dict__[field] != None):
                    context.user_data[field] = self.user.__dict__[field]
        elif ('id' in context.user_data.keys()):
            self.user = addUserIfUnique(User(**context.user_data))

        if (self.user != None):
            self.logger = sessionBase.getLogger(self.user.id)
        if (self.answer != None):
            self.logger.logMessage(str(self.answer), True)

    def replyText(self, text, keyboard = None):
        self.logger.logMessage(text)
        messageId = self.message.reply_text(text=formatMessage(text), reply_markup=keyboard)
        return messageId
    
    def editReplyText(self, messageId, text, log=False):
        if (log):
            self.logger.logMessage(text)
        self.bot.edit_message_text(chat_id=self.message.chat_id, message_id=messageId, text=formatMessage(text))

    def setAction(self, action: ChatAction):
        self.bot.send_chat_action(chat_id=self.message.chat_id, action=action)

    def sendMessage(self, text: str):
        self.bot.send_message(chat_id=self.user.id, text=formatMessage(text))

    def deleteMessage(self, messageId):
        self.bot.delete_message(chat_id=self.message.chat_id, message_id=messageId)

    def sendPhoto(self, url: str):
        with open(url, 'rb') as f:
            self.bot.send_photo(chat_id=self.user.id, photo=f)
            self.logger.logMessage(url)
    
    def editPhoto(self, messageId, url: str, caption = None):
        with open(url, 'rb') as f:
            self.bot.edit_message_media(chat_id=self.user.id, message_id=messageId, media=telegram.InputMediaPhoto(f))
            if (caption != None):
                self.bot.edit_message_caption(chat_id=self.user.id, message_id=messageId, caption=caption)
            self.logger.logMessage(url)

    def replyTextForAll(self, message):
        errorCount = 0
        users = getUsers()
        for user in users:
            try:
                if (user.id != ADMIN_ID):
                    self.bot.send_message(chat_id=user.id, text=message)
                    logSuccess("Сообщение отправлено пользователю: ", user)
            except telegram.error.Unauthorized:
                errorCount += 1
                logError("Oops! Чат с пользователем: ", user, ' не найден')
            except Exception as e:
                logError("Oops! ", user)

        resultStr = f'Рассылка пользователям сообщения: "{message}" завершена'
        resultStr += f', ошибок({errorCount})'
        resultStr += f', всего рассылок({len(users)})'
        tryCatch(lambda: self.bot.send_message(chat_id=ADMIN_ID, text=resultStr), 'Ошибка отправки сообщения с результатом рассылки')
    
    def threadReplyText(self, request):
        def operation():
            sent_message = self.replyText(getLoadingInclicator(self.user.id))
            self.setAction(ChatAction.TYPING)
            message_id = sent_message.message_id

            stoped = False
            def onStream():
                self.editReplyText(message_id, getLoadingInclicator(self.user.id))
                self.setAction(ChatAction.TYPING)
                time.sleep(2)
                if (stoped == False):
                    startAsync(onStream)
            startAsync(onStream)  

            result = request(self)
            stoped = True
            self.editReplyText(message_id, result, log=True)

        return threading.Thread(target=operation, args=()).start()
    
    def threadReplyImage(self, request):
        def operation():
            sent_message = self.replyText(getLoadingInclicator(self.user.id))
            self.setAction(ChatAction.UPLOAD_PHOTO)
            message_id = sent_message.message_id

            stoped = False
            def onStream():
                self.editReplyText(message_id, getLoadingInclicator(self.user.id))
                self.setAction(ChatAction.UPLOAD_PHOTO)
                time.sleep(2)
                if (stoped == False):
                    startAsync(onStream)
            startAsync(onStream)   

            result = request(self)
            if ('.jpg' in result):
                stoped = True
                self.sendPhoto(url=result)
            else:
                stoped = True
                self.sendMessage(result)
            self.deleteMessage(message_id)

        return threading.Thread(target=operation, args=()).start()

def updateContextProvider(fn):
    def provider(update, context):
        try:
            return fn(ChatManager(update=update, context=context))
        except Exception as e:
            logError(fn.__name__, e)
            return e
    return provider
    