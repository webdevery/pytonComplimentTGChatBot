from telegram import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton
from typing import List

# Buttons
class Button():
    text: str
    data: str
    def __init__(self, text, data):
        self.text = text
        self.data = data

class MarkUp():
    markup: ReplyKeyboardMarkup
    def __init__(self, lines: List[List[Button]], oneTimeKeyboard = False):
        buttons = []
        line = 0
        for buts in lines:
            buttons.append([])
            for but in buts:
                buttons[line].append(KeyboardButton(text=but.text, callback_data=but.data))
            line += 1

        self.markup = ReplyKeyboardMarkup(buttons, resize_keyboard=True, one_time_keyboard=oneTimeKeyboard)

class InlineMarkUp():
    markup: InlineKeyboardMarkup
    def __init__(self, lines: List[List[Button]]):
        buttons = []
        line = 0
        for buts in lines:
            buttons.append([])
            for but in buts:
                buttons[line].append(InlineKeyboardButton(text=but.text, callback_data=but.data))
            line += 1
        self.markup = InlineKeyboardMarkup(buttons)

# Создаем разметку для кнопок

class MarkUpManager():
    @staticmethod
    def createInlineMarkUp(params):
        lines = []
        index = 0
        for line in params:
            lines.append([])
            for item in line:
                lines[index].append(Button(item[0], item[1]))
            index += 1
        return InlineMarkUp(lines=lines)

    @staticmethod
    def createBaseMarkUp(params):
        lines = []
        index = 0
        for line in params:
            lines.append([])
            for item in line:
                lines[index].append(Button(item[0], item[1]))
            index += 1
        return MarkUp(lines=lines)
    
MALE = 'Парень'
FEMALE = 'Девушка'
markupSex = MarkUpManager.createInlineMarkUp([[[FEMALE, 1], [MALE, 0]]])