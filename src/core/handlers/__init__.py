from telegram.ext import Updater

from utils import logSuccess
from core.handlers.conversations import initConversations

def initHandlers(TOKEN: str):
    updater = Updater(token=TOKEN, use_context=True)
    dispatcher = updater.dispatcher
    initConversations(dispatcher=dispatcher)

    logSuccess('initHandlers')
    return updater
