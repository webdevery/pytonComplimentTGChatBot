from telegram.ext import CommandHandler

from core.manager import updateContextProvider, ChatManager
from core.handlers.commands import START_CONVERSATION

START_MESSAGE = 'Приветствую Вас, попрошу перед тем как мы начнем пройти небольшую регистрация, это сделает ваш опыт общения со мной более приятным!'

def startHandler(chat: ChatManager):
    chat.replyText(START_MESSAGE)

startConversationHandler = CommandHandler(command=START_CONVERSATION, callback=updateContextProvider(startHandler))