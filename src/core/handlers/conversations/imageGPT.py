from telegram.ext import MessageHandler, Filters, ConversationHandler, CommandHandler

from core.gpt.images import askImage
from core.manager import updateContextProvider, ChatManager
from core.handlers.commands import GPT_IMAGE_CONVERSATION, EXIT_CONVERSATION

START_GPT_MESSAGE = 'Бот может сгенерировать изображение по вашему запросу. Просто укажите, что вы хотите увидеть - фото или нарисованную картинку, а также какие детали должны быть на изображении. Бот использует свои алгоритмы для создания изображения, которое наилучшим образом соответствует вашему запросу. Учтите, что возможности бота ограничены его алгоритмами, поэтому результат может не всегда точно соответствовать вашим ожиданиям. Бот лучше понимает сообщения на английском языке.'
END_GPT_MESSAGE = f'Я всегда готов помочь вам. Вы можете вернуться к генерации изображений в любой момент, чтобы начать - выполните команду /{GPT_IMAGE_CONVERSATION}'
CONFIRM_EXIT = 'Чтобы завершить сессию генерацию изображений, выполните команду /{EXIT_CONVERSATION}'

def entryGptImageConversation(chat: ChatManager):
    chat.replyText(START_GPT_MESSAGE)
    return 'NEXT1'

def exitGptImageConversation(chat: ChatManager):
    chat.replyText(END_GPT_MESSAGE)
    return ConversationHandler.END

def exitGptImageConversation(chat: ChatManager):
    chat.replyText(END_GPT_MESSAGE)
    return ConversationHandler.END

def gptConversation1(chat: ChatManager):
    if (chat.answer.lower() == EXIT_CONVERSATION):
        return exitGptImageConversation(chat)
    else:
        chat.threadReplyImage(askImage)
        return 'NEXT2'

def gptConversation2(chat: ChatManager):
    if (chat.answer.lower() == EXIT_CONVERSATION):
        return exitGptImageConversation(chat)
    else:
        chat.threadReplyImage(askImage)
        return 'NEXT1'

imageGptConversationHandler = ConversationHandler(
    entry_points=[CommandHandler(GPT_IMAGE_CONVERSATION, updateContextProvider(entryGptImageConversation))],
    states={
        "NEXT1": [MessageHandler(Filters.text & ~Filters.command, updateContextProvider(gptConversation1))],
        "NEXT2": [MessageHandler(Filters.text & ~Filters.command, updateContextProvider(gptConversation2))],
    },
    fallbacks=[
        CommandHandler(EXIT_CONVERSATION, updateContextProvider(exitGptImageConversation)),
        MessageHandler(Filters.command & ~Filters.text(EXIT_CONVERSATION), updateContextProvider(exitGptImageConversation))
    ],
    allow_reentry=True
)