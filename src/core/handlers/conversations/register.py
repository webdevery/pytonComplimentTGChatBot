from telegram.ext import MessageHandler, Filters, ConversationHandler, CallbackQueryHandler, CommandHandler

from core.manager import sessionBase
from core.handlers.commands import REGISTER_CONVERSATION, GPT_CONVERSATION
from core.localize import PLS_HELP_ME_WITH_ADDITIOAN_BOT_CONFIGURATION

from models.types import User

from core.manager import updateContextProvider, ChatManager
from core.handlers.markup import markupSex

STEP_WRITE_NAME = 'Пожалуйста введите Ваше имя или то, как вы бы хотели чтобы бот к Вам обращался:'
STEP_SELECT_GENDER = 'Выберите ваш пол:'
STEP_WRITE_USER_CONTEXT = 'Вы также можете указать дополнительный контекст для бота, который станет вашим первым сообщением, и бот всегда будет его помнить. В этом сообщении вы можете указать свои предпочтения в общении или то, как вы бы хотели, чтобы бот обращался к вам.\n\nНапример, вы можете написать "Привет, я - Александра. Я бы предпочла, чтобы вы обращались ко мне на `ты` и использовали формулировки вежливости". В этом случае бот будет знать, как к вам обращаться и какую манеру общения использовать.\n\n'+PLS_HELP_ME_WITH_ADDITIOAN_BOT_CONFIGURATION
STEP_WRITE_SYSTEM_CONTEXT = 'Теперь вы можете настроить поведение чат-бота. Вы можете указать его манеру общения и предпочтения в обращении к вам. Например, если вы хотите, чтобы чат-бот использовал разговорный стиль общения и хвалил вас за упорство в задавании вопросов, вы можете указать это. Кроме того, вы можете уточнить, нужно ли чат-боту обьяснять сложные слова или давать подробные ответы.\n\nВот несколько примеров того, как можно настроить поведение чат-бота:\n- Используйте разговорный стиль общения и хвалите меня за мою настойчивость.\n- Если я задаю много вопросов, пожалуйста, напомните мне, что вы рады помочь и что все в порядке.\n- Если я не понимаю какое-то слово, объясните его мне.\n- Пожалуйста, предоставьте мне ответы подробно и ясно, чтобы я мог лучше понимать и запоминать информацию.\n\n'+PLS_HELP_ME_WITH_ADDITIOAN_BOT_CONFIGURATION

SUCCESS = f'Спасибо за регистрацию! Теперь мы можем начать общение, используйте команду /{GPT_CONVERSATION}'


def registerStart(chat: ChatManager):
    # chat.replyText(STEP_WRITE_NAME)
    # return "NAME"
    chat.replyText(STEP_SELECT_GENDER, markupSex.markup)
    return "GENDER"

def setGender(chat: ChatManager):
    gender = chat.answer

    text = STEP_WRITE_SYSTEM_CONTEXT
    chat.query.edit_message_text(text=text)
    chat.logger.logMessage(text)

    user: User = chat.user.clone()
    chat.context.user_data['sex'] = int(gender)
    user.sex = int(gender)
    sessionBase.updateUser(user)

    return "SYSTEM_CONTEXT"

def setSystemContext(chat: ChatManager):
    user: User = chat.user.clone()
    chat.context.user_data['systemGPTContext'] = chat.answer
    user.systemGPTContext = chat.answer
    sessionBase.updateUser(user)

    chat.replyText(STEP_WRITE_USER_CONTEXT)
    return "USER_CONTEXT"

def setUserContext(chat: ChatManager):
    user: User = chat.user.clone()
    chat.context.user_data['userGPTContext'] = chat.answer
    user.userGPTContext = chat.answer
    sessionBase.updateUser(user)
    
    return success(chat=chat)

def success(chat):
    user: User = chat.user.clone()
    user.sex = chat.context.user_data['sex']
    user.userGPTContext = chat.context.user_data['userGPTContext']
    user.systemGPTContext = chat.context.user_data['systemGPTContext']
    sessionBase.updateUser(user)

    chat.replyText(SUCCESS)
    return ConversationHandler.END

registerConversationHandler = ConversationHandler(
    entry_points=[CommandHandler(REGISTER_CONVERSATION, updateContextProvider(registerStart))],
    states={
        "GENDER": [CallbackQueryHandler(updateContextProvider(setGender))],
        "SYSTEM_CONTEXT": [MessageHandler(Filters.text & ~Filters.command, updateContextProvider(setSystemContext))],
        "USER_CONTEXT": [MessageHandler(Filters.text & ~Filters.command, updateContextProvider(setUserContext))]
    },
    fallbacks=[],
    allow_reentry=True
)