from telegram.ext import MessageHandler, Filters, ConversationHandler, CallbackQueryHandler, CommandHandler

from core.handlers.markup import MarkUpManager
from core.manager import ChatManager, sessionBase, updateContextProvider
from core.handlers.commands import PROFILE_CONVERSATION

import core.localize as Localize

from models.types import User

PROFILE_EDIT_NAME = 'Изменить имя'
PROFILE_EDIT_SYSTEM_CONTEXT = 'Изменить системный контекст'
PROFILE_EDIT_USER_CONTEXT = 'Изменить контекст пользователя'

changeName = 'changeUserName'
changeSysContext = 'changeSysContext'
changeUserContext = 'changeUserContext'

markupEditProfile = MarkUpManager.createInlineMarkUp([
    [[PROFILE_EDIT_NAME, changeName]],
    [[PROFILE_EDIT_SYSTEM_CONTEXT, changeSysContext]],
    [[PROFILE_EDIT_USER_CONTEXT, changeUserContext]]])

def profileHandler(chat: ChatManager):
    user = chat.user
    def field(label, val):
        return f'\n\n⚙️ {label}: \n- {val}'
    
    answer = f'{field("Системный контекст", user.systemGPTContext)}{field("Пользовательский контекст", user.userGPTContext)}'
    chat.replyText(answer)

profileConversationHandler = CommandHandler(command=PROFILE_CONVERSATION, callback=updateContextProvider(profileHandler))

def _startEdit(value: str):
    def fn(chat: ChatManager):
        chat.sendMessage('Введите новое значение')
        return value
    return fn

def _editName(chat: ChatManager):
    print("TGHCN", chat.context.user_data)
    chat.context.user_data['name'] = chat.answer
    _success(chat=chat)
    return ConversationHandler.END

def _editSysContext(chat: ChatManager):
    chat.context.user_data['systemGPTContext'] = chat.answer
    _success(chat=chat)
    return ConversationHandler.END

def _editUserContext(chat: ChatManager):
    chat.context.user_data['userGPTContext'] = chat.answer
    
    _success(chat=chat)
    return ConversationHandler.END

def _success(chat):
    user: User = chat.user.clone()
    data = chat.context.user_data
    if (hasattr(data, 'sex')):
        user.sex = chat.context.user_data['sex']
    if (hasattr(data, 'name')):
        user.name = chat.context.user_data['name']
    if (hasattr(data, 'userGPTContext')):
        user.userGPTContext = chat.context.user_data['userGPTContext']
    if (hasattr(data, 'systemGPTContext')):
        user.systemGPTContext = chat.context.user_data['systemGPTContext']

    chat.user = user

    sessionBase.updateUser(user)

    chat.replyText(Localize.REGISTER_SUCCESS)

def _createChangeFieldHandler(entry, start, handler, value):
    return ConversationHandler(
        entry_points=[CallbackQueryHandler(pattern=entry, callback=updateContextProvider(start(value)))],
        states={
            value: [MessageHandler(Filters.text & ~Filters.command, updateContextProvider(handler))],
        },
        fallbacks=[],
        allow_reentry=True,
        per_message=True
    )

editNameHandler = _createChangeFieldHandler(changeName, _startEdit, _editName, 'EDIT_NAME')
editSysContextHandler = _createChangeFieldHandler(changeSysContext, _startEdit, _editSysContext, 'EDIT_SYS_CONTEXT')
editUserContextHandler = _createChangeFieldHandler(changeUserContext, _startEdit, _editUserContext, 'EDIT_USER_CONTEXT')