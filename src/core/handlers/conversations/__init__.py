from telegram.ext import Dispatcher

from core.handlers.conversations.start import startConversationHandler
from core.handlers.conversations.register import registerConversationHandler
from core.handlers.conversations.chatGPT import chatGptConversationHandler
from core.handlers.conversations.imageGPT import imageGptConversationHandler

from core.handlers.conversations.profile import profileConversationHandler, editNameHandler, editSysContextHandler, editUserContextHandler

def initConversations(dispatcher: Dispatcher):
    dispatcher.add_handler(startConversationHandler)
    dispatcher.add_handler(registerConversationHandler)

    dispatcher.add_handler(profileConversationHandler)
    dispatcher.add_handler(chatGptConversationHandler)
    dispatcher.add_handler(imageGptConversationHandler)

    dispatcher.add_handler(editNameHandler)
    dispatcher.add_handler(editSysContextHandler)
    dispatcher.add_handler(editUserContextHandler)