from telegram.ext import MessageHandler, Filters, ConversationHandler, CommandHandler

from core.gpt.chat import askGPT
from core.manager import updateContextProvider, ChatManager
from core.handlers.commands import GPT_CONVERSATION, EXIT_CONVERSATION

START_GPT_MESSAGE = 'Как чат-бот, я готов помочь вам в любых вопросах и ситуациях. Я могу дать вам советы по любой теме, поделиться информацией и дать ответы на ваши вопросы. Если вы чувствуете себя неуверенно, заблудились в проблемах или нуждаетесь в мотивации, то я могу похвалить вас за достижения и поддержать вас в трудный момент. Вы можете задавать мне любые вопросы, и я постараюсь ответить на них максимально полно и точно. Я здесь, чтобы помочь вам и поддержать вас в любой ситуации. Не стесняйтесь обращаться ко мне и задавать вопросы!'
END_GPT_MESSAGE = f'Я всегда готов Вам помочь. Пожалуйста, не стесняйтесь обращаться ко мне за помощью, чтобы начать чат - выполните команду /{GPT_CONVERSATION}'
CONFIRM_EXIT = 'Чтобы завершить сессию чата GPT выполните команду /{EXIT_CONVERSATION}'

def entryGptConversation(chat: ChatManager):
    chat.replyText(START_GPT_MESSAGE)
    return 'NEXT1'

def exitGptConversation(chat: ChatManager):
    chat.replyText(END_GPT_MESSAGE)
    return ConversationHandler.END

def exitGptConversation(chat: ChatManager):
    chat.replyText(END_GPT_MESSAGE)
    return ConversationHandler.END

def gptConversation1(chat: ChatManager):
    if (chat.answer.lower() == EXIT_CONVERSATION):
        return exitGptConversation(chat)
    else:
        chat.threadReplyText(askGPT)
        return 'NEXT2'

def gptConversation2(chat: ChatManager):
    if (chat.answer.lower() == EXIT_CONVERSATION):
        return exitGptConversation(chat)
    else:
        chat.threadReplyText(askGPT)
        return 'NEXT1'

chatGptConversationHandler = ConversationHandler(
    entry_points=[CommandHandler(GPT_CONVERSATION, updateContextProvider(entryGptConversation))],
    states={
        "NEXT1": [MessageHandler(Filters.text & ~Filters.command, updateContextProvider(gptConversation1))],
        "NEXT2": [MessageHandler(Filters.text & ~Filters.command, updateContextProvider(gptConversation2))],
    },
    fallbacks=[
        CommandHandler(EXIT_CONVERSATION, updateContextProvider(exitGptConversation)),
        MessageHandler(Filters.command & ~Filters.text(EXIT_CONVERSATION), updateContextProvider(exitGptConversation))
    ],
    allow_reentry=True
)