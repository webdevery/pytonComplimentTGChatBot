LOADING = 'Загрузка, подождите немного...'
PROCCESSING_ERROR = 'Возникла ошибка при обработке сообщения, попробуйте позже или обратитесь в поддержку: @webdevery'

PLS_HELP_ME_WITH_ADDITIOAN_BOT_CONFIGURATION = 'Пожалуйста, сообщите мне(@webdevery), если у вас есть какие-либо дополнительные требования или пожелания, и я постараюсь настроить чат-бота соответствующим образом'

ACCESS_ERROR = 'У вас нет прав на выполнение этой команды, если вы столкнулись с ошибкой, обратитесь в поддержку: @webdevery'

