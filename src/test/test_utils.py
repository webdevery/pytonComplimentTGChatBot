import unittest
from io import StringIO
import sys

from src.utils import log, tryCatch

class TestUtilsMethods(unittest.TestCase):

    def testLog(self):
        test_cases = [
            ('logtype', ('test', [1,2,3], { 'a': { 'b': 'c' }}), "[logtype]: [test] - [1, 2, 3] {'a': {'b': 'c'}}"),
            ('logtype2', ('test2',), "[logtype2]: [test2] -"),
        ]
        for a, b, expected_result in test_cases:
            with self.subTest(a=a, b=b, expected_result=expected_result):
                captured_output = StringIO()
                sys.stdout = captured_output

                log(a, *b)

                output = captured_output.getvalue().strip()
                sys.stdout = sys.__stdout__

                self.assertEqual(output, expected_result)

    def testTryCatch(self):
        def testFn(a, b):
            return tryCatch(lambda: a / b)
        self.assertEqual(testFn(10, 0), None)
        self.assertEqual(testFn(10, 'a'), None)
        self.assertEqual(testFn(10, 1), 10)

if __name__ == '__main__':
    unittest.main()