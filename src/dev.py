from core import DBInit
from core.handlers import initHandlers
from utils import logSuccess

TOKEN = '6291350021:AAEO8riTd_lLZ7Ff41uaHyhmfALuDQXhD4Y'

def main():
    try:
        DBInit()
        
        # запускаем бота
        updater = initHandlers(TOKEN)
        updater.start_polling()
        updater.idle()
    except Exception as e:
        logSuccess("Ошиба при инициализации бота", e)

if __name__ == "__main__":
    main()