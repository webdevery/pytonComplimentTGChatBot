from core import DBInit
from core.handlers import initHandlers
from utils import logSuccess

TOKEN = '6165961468:AAHYVS4VNjIrfb1I3qkFl9N1ifFVL8_92KM'

def main():
    try:
        DBInit()
        
        # запускаем бота
        updater = initHandlers(TOKEN)
        updater.start_polling()
        updater.idle()
    except Exception as e:
        logSuccess("Ошиба при инициализации бота", e)

if __name__ == "__main__":
    main()