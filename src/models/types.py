import time

from typing import Literal, TypedDict, TypeVar, Generic
from utils import formatMessage

class User:
    id: int
    username: str
    first_name: str
    last_name: str
    name: str
    sex: Literal[1, 0]
    userGPTContext: str
    systemGPTContext: str

    def __init__(self, id, username,
                 first_name, last_name,
                 name = None, sex = None,
                 userGPTContext = None,
                 systemGPTContext = None):
        self.id = id
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.name = formatMessage(name) if name else None
        self.sex = sex
        self.userGPTContext = formatMessage(userGPTContext) if userGPTContext else None
        self.systemGPTContext = formatMessage(systemGPTContext) if systemGPTContext else None

    def fullName(self):
        return f'{self.first_name} {self.last_name}'
  
    def clone(self):
        return User(**self.__dict__)

T = TypeVar('T')
class ValueCache(Generic[T]):
    _value: T
    time: int
    def __init__(self, value: T):
        self._value = value
        self.time = time.time()

    @property
    def value(self) -> T:
        self.time = time.time()
        return self._value
    
    def updateValue(self, value: T):
        self.time = time.time()
        self._value = value

class GPTMessage(TypedDict):
    role: 'system' or 'assistent' or 'user'
    content: str
