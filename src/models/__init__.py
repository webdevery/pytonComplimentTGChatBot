import time
from typing import Dict
from models.types import User, ValueCache
from core.ydb import getUser, updateUser, addUser
from thread import startAsync
from utils import Log

class SessionBase():
    users: Dict[int, ValueCache[User]]
    loggers: Dict[int, ValueCache[Log]]

    def __init__(self):
        self.users = {}
        self.loggers = {}
    
    def addLogger(self, id: int):
        self.loggers[id] = ValueCache(Log(id))

    def addUser(self, user: User, isNew = False):
        self.users[user.id] = ValueCache(user)
        self.addLogger(user.id)
        if (isNew):
            startAsync(lambda: addUser(user))

    def getUser(self, id):
        if (id in self.users):
            return self.users[id].value
        else:
            user = getUser(id)
            if (user != None):
                self.addUser(user)
                return self.getUser(id)
            return None

    def getLogger(self, id):
        if (self.getUser(id) != None):
            return self.loggers[id].value
        else:
            return None

    def createNewOrGetExistedUser(self, user) -> User:
        userFromDB = getUser(user.id)
        if (userFromDB != None):
            self.addUser(userFromDB)
            return self.getUser(user.id)
        else:
            u = User(user.id, user.username, user.first_name, user.last_name)
            self.addUser(user=u, isNew=True)
            return u

    def updateUser(self, user: User):
        if (user.id in self.users):
            self.users[user.id].updateValue(user) 
        else:
            self.addUser(user)
        startAsync(lambda: updateUser(user))

    def clearCache(self):
        current_time = time.time()
        for key in list(self.users.keys()):
            if current_time - self.users[key].time >= 3600:
                del self.users[key]
        for key in list(self.loggers.keys()):
            if current_time - self.loggers[key].time >= 3600:
                del self.loggers[key]
