import logging
import requests
import datetime
import os
import core.localize as Localize
import re

from logging import Logger

LOGS_PATH = './logs'
IMAGES_PATH = './generatedImages'

def getNow():
    now = datetime.datetime.now()
    return now.strftime('%Y-%m-%d')

def createPathIfNotExist(path: str):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

def saveFile(url, name):
    fResp = requests.get(url)
    fileName = f"{IMAGES_PATH}/{name}"
    createPathIfNotExist(fileName)
    with open(fileName, "wb") as f:
        f.write(fResp.content)

    return fileName

logMessageFormatter = logging.Formatter('_________\n%(asctime)s\n_________n%(message)s \n')
logInfoFormatter    = logging.Formatter('%(asctime)s - %(message)s')

def getLogger(name: str, formatter: logging.Formatter) -> Logger:
    filePath = f'{LOGS_PATH}/{name}.log'
    createPathIfNotExist(filePath)

    loggerHandler = logging.FileHandler(filePath, encoding='utf-8')
    loggerHandler.setFormatter(formatter)
    loggerHandler.setLevel(logging.INFO)
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    logger.addHandler(loggerHandler)

    return logger

infoLogger  = getLogger(f'info_.logs.{getNow()}', logInfoFormatter)

INFO    = '  INFO '
SUCCESS = 'SUCCESS'
ERROR   = ' ERROR '

class Log:
    logger: Logger
    def __init__(self, id):
        self.logger = getLogger(f'{id}/messages.{getNow()}', logMessageFormatter)

    def logMessage(self, message: str, my: bool = False):
        text = formatMessage(re.sub("\n\n+", "\n\n", message))
        logText = f'[{"USER" if my else "BOT"}]: {text}'
        print(f'[MESSAGE] {logText}')
        self.logger.info(logText)

def log(logoType, *messages):
    message = ''
    for i in range(len(messages)):
        if (i == 0):
            message += f'[{messages[i]}] - '
        else:
            message += f'{messages[i]} '

    message = formatMessage(message)
    infoLogger.info(f'[{logoType}]: {message}')
    print(f'[{logoType}]: {message}')

def logInfo(*messages):
    log(INFO, *messages)

def logSuccess(*messages):
    log(SUCCESS, *messages)

def logError(*messages):
    log(ERROR, *messages)

def tryCatch(fn, message = None):
    try:
        return fn()
    except Exception as e:
        logError(fn.__name__, message, e)
        return None

def formatMessage(text: str):
    return requests.utils.unquote(text).strip(' \t\n\r')

memo = {}
def getLoadingInclicator(id: int):
    if (id in memo):
        memo[id] += 1
    else:
        memo[id] = 0
    index = memo[id]
    arr = '🕛,🕐,🕑,🕒,🕓,🕔,🕕,🕖,🕗,🕘,🕙,🕚'.split(',')
    return arr[index % len(arr)] + ' ' + Localize.LOADING